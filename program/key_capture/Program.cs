﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Reflection.Emit;

class KeyCapture
{
    private const int WH_KEYBOARD_LL = 13;
    private const int WM_KEYDOWN = 0x0100;
    private const int WM_KEYUP = 0x0101;
    private static LowLevelKeyboardProc _proc = HookCallback;
    private static IntPtr _hookID = IntPtr.Zero;

    static bool key_up = false;
    static bool key_down = false;
    static bool key_left = false;
    static bool key_right = false;

    public static void Main()
    {
        _hookID = SetHook(_proc);
        Application.Run();
        UnhookWindowsHookEx(_hookID);
    }

    private static IntPtr SetHook(LowLevelKeyboardProc proc)
    {
        using (Process curProcess = Process.GetCurrentProcess())
        using (ProcessModule curModule = curProcess.MainModule)
        {
            return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                GetModuleHandle(curModule.ModuleName), 0);
        }
    }

    private delegate IntPtr LowLevelKeyboardProc(
        int nCode, IntPtr wParam, IntPtr lParam);

    private static IntPtr HookCallback(
        int nCode, IntPtr wParam, IntPtr lParam)
    {
        int vkCode = Marshal.ReadInt32(lParam);
        //Console.WriteLine((Keys)vkCode);
        if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
        {
            if( vkCode == 38 && KeyCapture.key_up == false )
            {
                KeyCapture.key_up = true;
                Console.WriteLine(vkCode + " " + (Keys)vkCode + " pressed");
            }
            else if (vkCode == 37 && KeyCapture.key_left == false)
            {
                KeyCapture.key_left = true;
                Console.WriteLine(vkCode + " " + (Keys)vkCode + " pressed");
            }
            else if (vkCode == 39 && KeyCapture.key_right == false)
            {
                KeyCapture.key_right = true;
                Console.WriteLine(vkCode + " " + (Keys)vkCode + " pressed");
            }
            else if (vkCode == 40 && KeyCapture.key_down == false)
            {
                KeyCapture.key_down = true;
                Console.WriteLine(vkCode + " " + (Keys)vkCode + " pressed");
            }
        }
        else if(  nCode >= 0 && wParam == (IntPtr)WM_KEYUP)
        {
            if (vkCode == 38 && KeyCapture.key_up )
            {
                KeyCapture.key_up = false;
                Console.WriteLine(vkCode + " " + (Keys)vkCode + " released");
            }
            else if (vkCode == 37 && KeyCapture.key_left )
            {
                KeyCapture.key_left = false;
                Console.WriteLine(vkCode + " " + (Keys)vkCode + " released");
            }
            else if (vkCode == 39 && KeyCapture.key_right )
            {
                KeyCapture.key_right = false;
                Console.WriteLine(vkCode + " " + (Keys)vkCode + " released");
            }
            else if (vkCode == 40 && KeyCapture.key_down )
            {
                KeyCapture.key_down = false;
                Console.WriteLine(vkCode + " " + (Keys)vkCode + " released");
            }
        }

        return CallNextHookEx(_hookID, nCode, wParam, lParam);
    }

    [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    private static extern IntPtr SetWindowsHookEx(int idHook,
        LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

    [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool UnhookWindowsHookEx(IntPtr hhk);

    [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
        IntPtr wParam, IntPtr lParam);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    private static extern IntPtr GetModuleHandle(string lpModuleName);
}