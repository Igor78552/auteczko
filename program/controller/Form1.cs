using System;
using System.Runtime;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.CodeDom;

namespace controller
{
    public partial class Form1 : Form
    {
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;
        private const int WM_KEYUP = 0x0101;
        private static LowLevelKeyboardProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;

        static bool key_up = false;
        static bool key_down = false;
        static bool key_left = false;
        static bool key_right = false;

        TCPClientThread? tcp_client = null;

        // holds movement control parameters for tcp thread.
        Control control = new Control();

        
        static TextBox text1;
        public Form1()
        {
            
            _hookID = SetHook(_proc);

            InitializeComponent();

            text1 = this.textBox1;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess?.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        public delegate IntPtr LowLevelKeyboardProc( int nCode, IntPtr wParam, IntPtr lParam);

        public static IntPtr HookCallback( int nCode, IntPtr wParam, IntPtr lParam)
        {
            int vkCode = Marshal.ReadInt32(lParam);

            if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
            {
                if (vkCode == 38 && key_up == false)
                {
                    key_up = true;
                    text1.Text += vkCode + " " + (Keys)vkCode + " pressed" + Environment.NewLine;
                }
                else if (vkCode == 37 && key_left == false)
                {
                    key_left = true;
                    text1.Text += vkCode + " " + (Keys)vkCode + " pressed" + Environment.NewLine;
                }
                else if (vkCode == 39 && key_right == false)
                {
                    key_right = true;
                    text1.Text += vkCode + " " + (Keys)vkCode + " pressed" + Environment.NewLine;
                }
                else if (vkCode == 40 && key_down == false)
                {
                    key_down = true;
                    text1.Text += vkCode + " " + (Keys)vkCode + " pressed" + Environment.NewLine;
                }
            }
            else if (nCode >= 0 && wParam == (IntPtr)WM_KEYUP)
            {
                if (vkCode == 38 && key_up)
                {
                    key_up = false;
                    text1.Text += vkCode + " " + (Keys)vkCode + " released" + Environment.NewLine;
                }
                else if (vkCode == 37 && key_left)
                {
                    key_left = false;
                    text1.Text += vkCode + " " + (Keys)vkCode + " released" + Environment.NewLine;
                }
                else if (vkCode == 39 && key_right)
                {
                    key_right = false;
                    text1.Text += vkCode + " " + (Keys)vkCode + " released" + Environment.NewLine;
                }
                else if (vkCode == 40 && key_down)
                {
                    key_down = false;
                    text1.Text += vkCode + " " + (Keys)vkCode + " released" + Environment.NewLine;
                    
                }
            }

            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            UnhookWindowsHookEx(_hookID);
            //Closing tcp thread

            lock (control.ObjLock)
            {
                control.close_request = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
        LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
            IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        private void button1_Click(object sender, EventArgs e)
        {
            if( this.button1.Enabled )
            {
                lock (control.ObjLock)
                {
                    control.connected = Control.ConnectionState.Connecting;
                }

                timer1.Start();
                this.button1.Enabled = false;
                String address = textBox2.Text;
                tcp_client = new TCPClientThread( control,
                address, 54545);

                Thread t = new Thread(new ThreadStart(tcp_client.ThreadProc));
                t.Start();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UInt32 rx, tx;
            
            Control.ConnectionState connect_state_current = Control.ConnectionState.Disconnected;
            lock (control.ObjLock)
            {
                rx = control.rx;
                tx = control.tx;
                connect_state_current = control.connected;

                control.left_wh_dir = 0;
                control.left_speed = 0;
                control.right_wh_dir = 0;
                control.right_speed = 0;


                if (key_right)
                {
                    control.right_wh_dir = -1;
                    control.left_wh_dir = 1;
                    control.left_speed = 10;
                    control.right_speed = 10;
                }
                if (key_left)
                {
                    control.right_wh_dir = 1;
                    control.left_wh_dir = -1;
                    control.left_speed = 10;
                    control.right_speed = 10;
                }
                if (key_up)
                {
                    control.right_wh_dir = 1;
                    control.left_wh_dir = 1;
                    control.left_speed = 10;
                    control.right_speed = 10;
                }
                if (key_down)
                {
                    control.right_wh_dir = -1;
                    control.left_wh_dir = -1;
                    control.left_speed = 10;
                    control.right_speed = 10;
                }
            }
            label3.Text = tx.ToString();
            label4.Text = rx.ToString();

            if( connect_state_current == Control.ConnectionState.Disconnected)
            {
                button1.Enabled = true;
                button1.Text = "Connect";
            }

            if(connect_state_current == Control.ConnectionState.Connecting )
            {
                button1.Text = "Connecting...";
            }

            if (connect_state_current == Control.ConnectionState.Connected)
            {
                button1.Text = "Connected";
                button1.Enabled = false;
            }
        }
    }
}