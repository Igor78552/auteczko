using System.Diagnostics;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace controller
{

    public class Control
    {
        public enum ConnectionState
        {
            Connected,
            Disconnected,
            Connecting
        }

        public object ObjLock;
        public Int32 left_wh_dir { get; set; }
        public UInt32 left_speed { get; set; }
        public Int32 right_wh_dir { get; set; }
        public UInt32 right_speed { get; set; }
        public UInt32 rx { get; set; }
        public UInt32 tx { get; set; }
        public bool close_request { get; set; }
    public ConnectionState connected { get; set; }

        public Control()
        {
            ObjLock = new object();
            rx = 0;
            tx = 0;
            connected = ConnectionState.Disconnected;
        }
    }

    public struct Message
    {
        public UInt32 flag { get; set; } 
        public UInt32 length { get; set; }
        public UInt32 crc32 { get; set; }
        public UInt32 time { get; set; }
        public Int32 left_wh_dir { get; set; }
        public UInt32 left_speed { get; set; }
        public Int32 right_wh_dir { get; set; }
        public UInt32 right_speed { get; set; }

        public Message(Int32 ldir, UInt32 lspeed, Int32 rdir, UInt32 rspeed )
        {
            flag = 0xDEADBEEF;
            crc32 = 0;
            time = 0;
            length = 5 * sizeof(UInt32) + 2 * sizeof(Int32);
            left_wh_dir = ldir;
            left_speed = lspeed;
            right_wh_dir = rdir;
            right_speed = rspeed;
            
           // UInt32 time = Convert.ToUInt32(DateTimeOffset.Now.ToUnixTimeMilliseconds());
        }

        
        public byte[] getBytes(Message data)
        {
            int size = Marshal.SizeOf(data);
            byte[] arr = new byte[size];

            IntPtr ptr = IntPtr.Zero;
            try
            {
                ptr = Marshal.AllocHGlobal(size);
                Marshal.StructureToPtr(data, ptr, true);
                Marshal.Copy(ptr, arr, 0, size);
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
            return arr;
        }

        public override string ToString() => $"({flag}, {length}, {left_wh_dir}, {left_speed}, {right_wh_dir}, {right_speed})";
    }

    public class TCPClientThread
    {

        String server;
        Int32 port;
        TcpClient client;
        bool loop = true;
        Control control;

        public TCPClientThread(Control control, String server, Int32 port)
        {
            this.server = server;
            this.port = port;
            this.control = control;
            client = new TcpClient();
        }

        public void ThreadProc()
        {
            

            try
            {
                var result = client.BeginConnect(server, port, null, null);
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(1000));
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine("ArgumentOutOfRangeException: {0}", e);
            }
            catch (ObjectDisposedException e)
            {
                Console.WriteLine("ObjectDisposedException: {0}", e);
            }

            if (client.Connected)
            {
                // Get a client stream for reading and writing.
                NetworkStream stream = client.GetStream();

                lock (control.ObjLock)
                {
                    control.connected = Control.ConnectionState.Connected;
                    loop = !control.close_request;
                }

                while (loop)
                {

                    Message tx_msg = new Message(0, 0, 0, 0);

                    lock (control.ObjLock)
                    {
                        tx_msg.left_wh_dir = control.left_wh_dir;
                        tx_msg.left_speed = control.left_speed;
                        tx_msg.right_wh_dir = control.right_wh_dir;
                        tx_msg.right_speed = control.right_speed;
                        control.tx += tx_msg.length;
                        loop = !control.close_request;
                    }

                    Byte[] tx_data = tx_msg.getBytes(tx_msg);
                    Byte[] rx_data = new Byte[1024];

                    try
                    {
                        // Send the message to the connected TcpServer.
                        stream.Write(tx_data, 0, tx_data.Length);

                        //Read data from the stream.
                        Int32 bytes = stream.Read(rx_data, 0, rx_data.Length);

                        lock (control.ObjLock)
                        {
                            control.rx += unchecked((UInt32)bytes);
                        }
                    }
                    catch (ArgumentNullException e)
                    {
                        Console.WriteLine("ArgumentNullException: {0}", e);
                        loop = false;
                    }
                    catch (SocketException e)
                    {
                        Console.WriteLine("SocketException: {0}", e);
                        loop = false;
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine("System.IO.IOException: {0}", e);
                        loop = false;
                    }

                    if (loop)
                    {
                        Thread.Sleep(50);
                    }
                }

                stream.Close();
                client.Close();

                lock (control.ObjLock)
                {
                    control.connected = Control.ConnectionState.Disconnected;
                    control.close_request = false;
                }
            }
        }
    }

    public class Program
    {
        [STAThread]
        static void Main()
        {
            ApplicationConfiguration.Initialize();
            Application.Run(new Form1());
        }
    }
}