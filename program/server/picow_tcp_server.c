/**
 * Copyright (c) 2022 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <string.h>
#include <stdlib.h>

#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"

#include "lwip/pbuf.h"
#include "lwip/tcp.h"

#include "hardware/pwm.h"
#include "proto.h"

#define TCP_PORT 54545
#define DEBUG_printf printf
#define BUF_SIZE 2048
#define TEST_ITERATIONS 10
#define POLL_TIME_S 5
#define FIRST_L298N 6

enum Motor
{
    MOTOR_A = 0,
    MOTOR_B 
};

enum Direction
{
    FORWARD = 0,
    BACKWARD,
    STOP
};

typedef struct Control_t
{
    uint8_t left_wheel_dir;
    uint8_t left_wheel_speed;
    uint8_t right_wheel_dir;
    uint8_t right_wheel_speed;
} Control;

typedef struct TCP_SERVER_T_ {
    struct tcp_pcb *server_pcb;
    struct tcp_pcb *client_pcb;
    bool complete;
    uint8_t buffer_sent[BUF_SIZE];
    uint8_t buffer_recv[BUF_SIZE];
    int sent_len;
    int recv_len;
    int run_count;
    int recv_pos;
    uint32_t send_buffer_size;
} TCP_SERVER_T;

static TCP_SERVER_T* tcp_server_init(void) {
    TCP_SERVER_T *state = calloc(1, sizeof(TCP_SERVER_T));
    if (!state) {
        DEBUG_printf("failed to allocate state\n");
        return NULL;
    }
    return state;
}

void SetMotor( enum Motor motor, enum Direction dir );
void ProcessMessage( Message_t* msg );


static err_t tcp_server_close(void *arg) {
    TCP_SERVER_T *state = (TCP_SERVER_T*)arg;
    state->send_buffer_size = 0;
    err_t err = ERR_OK;
    if (state->client_pcb != NULL) {
        tcp_arg(state->client_pcb, NULL);
        tcp_poll(state->client_pcb, NULL, 0);
        tcp_sent(state->client_pcb, NULL);
        tcp_recv(state->client_pcb, NULL);
        tcp_err(state->client_pcb, NULL);
        err = tcp_close(state->client_pcb);
        if (err != ERR_OK) {
            DEBUG_printf("close failed %d, calling abort\n", err);
            tcp_abort(state->client_pcb);
            err = ERR_ABRT;
        }
        state->client_pcb = NULL;
    }
    if (state->server_pcb) {
        tcp_arg(state->server_pcb, NULL);
        tcp_close(state->server_pcb);
        state->server_pcb = NULL;
    }
    return err;
}

static err_t  tcp_server_result(void *arg, int status) {
    TCP_SERVER_T *state = (TCP_SERVER_T*)arg;

    SetMotor( MOTOR_A, STOP );
    SetMotor( MOTOR_B, STOP );
    tcp_close( state->client_pcb);
    if (status == -14) {
        DEBUG_printf("Connection Lost \n");
    }
    else if (status == -3) {
        DEBUG_printf("Connection Lost \n");
    }
    else if (status == -1 ){
        DEBUG_printf("Error Read/Write %d\n", status);
    }
    return ERR_OK;
}

static err_t tcp_server_sent(void *arg, struct tcp_pcb *tpcb, u16_t len) {
    TCP_SERVER_T *state = (TCP_SERVER_T*)arg;
    //DEBUG_printf("tcp_server_sent %u\n", len);
    state->sent_len += len;

    if (state->sent_len >= BUF_SIZE) {

        // We should get the data back from the client
        state->recv_len = 0;
        //DEBUG_printf("Waiting for buffer from client\n");
    }

    return ERR_OK;
}

err_t tcp_server_send_data(void *arg, struct tcp_pcb *tpcb)
{
    TCP_SERVER_T *state = (TCP_SERVER_T*)arg;

    state->sent_len = 0;
    uint32_t sent_len;
    //DEBUG_printf("Writing %ld bytes to client\n", state->send_buffer_size);
    // this method is callback from lwIP, so cyw43_arch_lwip_begin is not required, however you
    // can use this method to cause an assertion in debug mode, if this method is called when
    // cyw43_arch_lwip_begin IS needed
    cyw43_arch_lwip_check();
    err_t err = tcp_write(tpcb, &sent_len, state->send_buffer_size, TCP_WRITE_FLAG_COPY);
    if (err != ERR_OK) {
        DEBUG_printf("Failed to write data %d\n", err);
        return tcp_server_result(arg, -1);
    }
    state->sent_len+=sent_len;
    state->send_buffer_size -=sent_len;

    return ERR_OK;
}

err_t tcp_server_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err) {
    TCP_SERVER_T *state = (TCP_SERVER_T*)arg;
    if (!p) {
        return tcp_server_result(arg, -1);
    }
    // this method is callback from lwIP, so cyw43_arch_lwip_begin is not required, however you
    // can use this method to cause an assertion in debug mode, if this method is called when
    // cyw43_arch_lwip_begin IS needed
    cyw43_arch_lwip_check();
    if (p->tot_len > 0) {
        //DEBUG_printf("tcp_server_recv %d/%d err %d\n", p->tot_len, state->recv_len, err);

        // Receive the buffer
        const uint16_t buffer_left = BUF_SIZE - state->recv_len;
        state->recv_len += pbuf_copy_partial(p, state->buffer_recv + state->recv_len,
                                             p->tot_len > buffer_left ? buffer_left : p->tot_len, 0);
        tcp_recved(tpcb, p->tot_len);
    }
    pbuf_free(p);

    // Have we have received the whole buffer
    state->send_buffer_size = 0;
    if(state->recv_len >= sizeof(Message_t))
    {
        Message_t msg;
        while(state->recv_len >= sizeof(Message_t))
        {
            // check if it matches
            memcpy( &msg, state->buffer_recv, sizeof(Message_t));

            ProcessMessage( &msg );
            state->recv_len -= sizeof(Message_t);

            memcpy( state->buffer_sent, &msg, sizeof(Message_t));
            state->send_buffer_size = sizeof(Message_t);
        }

        // Respond with the last received packet.
        return tcp_server_send_data(arg, state->client_pcb);
    }
    return ERR_OK;
}

static err_t tcp_server_poll(void *arg, struct tcp_pcb *tpcb) {
    DEBUG_printf("tcp_server_poll_fn\n");
    SetMotor( MOTOR_A, STOP );
    SetMotor( MOTOR_B, STOP );
    return tcp_server_result(arg, -3); // no response is an error?
}

static void tcp_server_err(void *arg, err_t err) {
    if (err != ERR_ABRT) {
        SetMotor( MOTOR_A, STOP );
        SetMotor( MOTOR_B, STOP );
        DEBUG_printf("tcp_client_err_fn %d\n", err);
        tcp_server_result(arg, err);
    }
}

static err_t tcp_server_accept(void *arg, struct tcp_pcb *client_pcb, err_t err) {
    TCP_SERVER_T *state = (TCP_SERVER_T*)arg;
    if (err != ERR_OK || client_pcb == NULL) {
        DEBUG_printf("Failure in accept\n");
        tcp_server_result(arg, err);
        return ERR_VAL;
    }
    DEBUG_printf("Client connected\n");

    state->client_pcb = client_pcb;
    tcp_arg(client_pcb, state);
    tcp_sent(client_pcb, tcp_server_sent);
    tcp_recv(client_pcb, tcp_server_recv);
    tcp_poll(client_pcb, tcp_server_poll, POLL_TIME_S * 2);
    tcp_err(client_pcb, tcp_server_err);

    return ERR_OK;//tcp_server_send_data(arg, state->client_pcb);
}

static bool tcp_server_open(void *arg) {
    TCP_SERVER_T *state = (TCP_SERVER_T*)arg;
    DEBUG_printf("Starting server at %s on port %u\n", ip4addr_ntoa(netif_ip4_addr(netif_list)), TCP_PORT);

    struct tcp_pcb *pcb = tcp_new_ip_type(IPADDR_TYPE_ANY);
    if (!pcb) {
        DEBUG_printf("failed to create pcb\n");
        return false;
    }

    err_t err = tcp_bind(pcb, NULL, TCP_PORT);
    if (err) {
        DEBUG_printf("failed to bind to port %d\n");
        return false;
    }

    state->server_pcb = tcp_listen_with_backlog(pcb, 1);
    if (!state->server_pcb) {
        DEBUG_printf("failed to listen\n");
        if (pcb) {
            tcp_close(pcb);
        }
        return false;
    }

    tcp_arg(state->server_pcb, state);
    tcp_accept(state->server_pcb, tcp_server_accept);

    return true;
}

void run_tcp_server_test(void) {
    TCP_SERVER_T *state = tcp_server_init();
    if (!state) {
        return;
    }
    if (!tcp_server_open(state)) {
        tcp_server_result(state, -1);
        return;
    }
    while(!state->complete) {
        // the following #ifdef is only here so this same example can be used in multiple modes;
        // you do not need it in your code
#if PICO_CYW43_ARCH_POLL
        // if you are using pico_cyw43_arch_poll, then you must poll periodically from your
        // main loop (not from a timer) to check for WiFi driver or lwIP work that needs to be done.
        cyw43_arch_poll();
        sleep_ms(1);
#else
        // if you are not using pico_cyw43_arch_poll, then WiFI driver and lwIP work
        // is done via interrupt in the background. This sleep is just an example of some (blocking)
        // work you might be doing.
        sleep_ms(10);
#endif
    }
    free(state);
}

void ProcessMessage( Message_t* msg )
{
    switch( msg->movement.left_wheel_dir )
    {
        case -1: { SetMotor( MOTOR_A, BACKWARD ); } break;
        case 0: { SetMotor( MOTOR_A, STOP ); } break;
        case 1: { SetMotor( MOTOR_A, FORWARD ); } break;
        default: {SetMotor( MOTOR_A, STOP ); }
    }

    switch( msg->movement.right_wheel_dir )
    {
        case -1: { SetMotor( MOTOR_B, BACKWARD ); } break;
        case 0: { SetMotor( MOTOR_B, STOP ); } break;
        case 1: { SetMotor( MOTOR_B, FORWARD ); } break;
        default: {SetMotor( MOTOR_B, STOP ); }
    }
}

void ConfigurePWM()
{
    // Tell GPIO 14 and 15 they are allocated to the PWM
    gpio_set_function(14, GPIO_FUNC_PWM);
    gpio_set_function(15, GPIO_FUNC_PWM);
    
    // Find out which PWM slice is connected to GPIO 14 (it's slice 0)
    uint slice_num = pwm_gpio_to_slice_num(14);

    // Set period of 4 cycles (0 to 3 inclusive)
    pwm_set_wrap(slice_num, 100);
    // Set channel A output high for one cycle before dropping
    pwm_set_chan_level(slice_num, PWM_CHAN_A, 60);
    // Set initial B output high for three cycles before dropping
    pwm_set_chan_level(slice_num, PWM_CHAN_B, 60);
    // Set the PWM running
    pwm_set_enabled(slice_num, true);
}

void ConfigureGPIO()
{
    for (int gpio = FIRST_L298N; gpio < FIRST_L298N + 4; gpio++) {
        gpio_init(gpio);
        gpio_set_dir(gpio, GPIO_OUT);
    }
}

void SetMotor( enum Motor motor, enum Direction dir )
{
    uint32_t pin0 = FIRST_L298N, pin1 = FIRST_L298N+1;
    bool vpin0, vpin1;
    if( FORWARD == dir )
    {
        vpin0 = false;
        vpin1 = true;
    }
    else if( BACKWARD == dir )
    {
        vpin0 = true;
        vpin1 = false;
    }
    else if( STOP == dir )
    {
        vpin0 = false;
        vpin1 = false;
    }

    if( motor == MOTOR_B )
    {
        pin0+=2;
        pin1+=2;
    }

    gpio_put(pin0,vpin0);
    gpio_put(pin1,vpin1);
}


int main() {
    stdio_init_all();

    ConfigureGPIO();
    ConfigurePWM();

    SetMotor( MOTOR_A, STOP );
    SetMotor( MOTOR_B, STOP );

    if (cyw43_arch_init()) {
        printf("failed to initialise\n");
        return 1;
    }

    cyw43_arch_enable_sta_mode();

    printf("Connecting to WiFi...\n");
    if (cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASSWORD, CYW43_AUTH_WPA2_AES_PSK, 60000)) {
        printf("failed to connect.\n");
        return 1;
    } else {
        printf("Connected.\n");
    }

    ConfigurePWM();

    run_tcp_server_test();
    cyw43_arch_deinit();
    return 0;
}