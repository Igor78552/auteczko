#ifndef PROTO_H
#define PROTO_H

#define COMAND_TURN_LEFT     0x00000001
#define COMAND_TURN_RIGHT    0x00000002
#define COMAND_MOVE_FOREWARD 0x00000003
#define COMAND_MOVE_BACKWORD 0x00000004

typedef struct Movement_t
{
    int32_t left_wheel_dir;
    uint32_t left_wheel_speed;
    int32_t right_wheel_dir;
    uint32_t right_wheel_speed;
} Movement;

typedef struct Message_t
{
    uint32_t flag;
    uint32_t message_length;
    uint32_t crc32;
    uint32_t time;
    Movement movement;
} Message_t;

#endif 